#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>

// GRL                                                                                                                                                                                                      
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>

//============ Filling Histograms ===============//                                                                                                                                                         

#include <TH1.h>

//========== for making N-tuples =======//                                                                                                                                                                  
#include <TTree.h>
#include <vector>
#include <TLorentzVector.h>

class MyxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ~MyxAODAnalysis () override;

 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
  
  /// Electron pT cut
  double m_electronPtCut;
  /// Sample name
  std::string m_sampleName;

  unsigned int m_runNumber = 0; ///< Run number                                             
  unsigned long long m_eventNumber = 0; ///< Event number
  
  std::vector<float> *m_particleStatus = nullptr;
  std::vector<float> *m_particlePdgID =nullptr;
  std::vector<float> *m_particleEta = nullptr;
  std::vector<float> *m_particlePhi = nullptr;
  std::vector<float> *m_particlePt = nullptr;
  std::vector<float> *m_particleE = nullptr;
  std::vector<float> *m_particlePx = nullptr;
  std::vector<float> *m_particlePy = nullptr;
  std::vector<float> *m_particlePz = nullptr;
  
};

#endif
