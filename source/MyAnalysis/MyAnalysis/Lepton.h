#ifndef LEPTON_H
#define LEPTON_H

#include <iostream>
#include <fstream>
#include <cmath>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TLine.h"
#include "TLorentzVector.h"
#include <vector>
#include "TGraph.h"
#include <fstream>

#include "TString.h"
#include "THStack.h"
#include <math.h>
#include <TLine.h>
#include <TArrow.h>
#include <TLegend.h>

class Lepton {

 private: 
  
  // member variables
  
  TLorentzVector m_TLorentzVector;
  int m_PDGID;
  int m_ParticleStatus;
  double m_MaxEta;
  double m_MinPt;
  double m_Pt;
  double m_Px, m_Py, m_Pz, m_E, m_M;

  // member functions

 public:

  Lepton(int a_PDGID, int a_ParticleStatus, double a_E, double a_Px, double a_Py, double a_Pz, double a_MaxEta, double a_MinPt)
  {
    set_PDGID(a_PDGID);
    set_ParticleStatus(a_ParticleStatus);
    set_TLorentzVector(a_Px, a_Py, a_Pz, a_E);
    set_MaxEta(a_MaxEta);
    set_MinPt(a_MinPt);
    set_M();
    set_E(a_E);
    set_Px(a_Px);
    set_Py(a_Py);
    set_Pz(a_Pz);
    set_E(a_E);
    set_Pt();
  };

  ~Lepton(){};
  void Init();
  void set_PDGID(int PDGID);
  void set_ParticleStatus(int ParticleStatus);
  void set_TLorentzVector(double Px, double Py, double Pz, double E);
  void set_MaxEta(double Eta);
  void set_MinPt(double Pt);
  void set_Px(double Px);
  void set_Py(double Py);
  void set_Pz(double Pz);
  void set_E(double E);
  void set_M();
  void set_Pt();
  
  int PDGID();
  int ParticleStatus();
  TLorentzVector GetTLorentzVector();
  double MaxEta();
  double MinPt();
  double M();
  double Px();
  double Py();
  double Pz();
  double E();
  double Pt();
  double Eta();
  double Phi();
  double Rho();
  double Theta();
  double CosTheta();
};

#endif
