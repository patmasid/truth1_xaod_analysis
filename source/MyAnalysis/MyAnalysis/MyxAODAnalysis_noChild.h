#ifndef MyAnalysis_MyxAODAnalysis_noChild_H
#define MyAnalysis_MyxAODAnalysis_noChild_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <MyAnalysis/Lepton.h>
// GRL                                                                                                                                                                                                      
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>

//============ Filling Histograms ===============//                                                                                                                                                         

#include <TH1.h>
#include <cmath>
//========== for making N-tuples =======//                                                                                                                                                                  
#include <TTree.h>
#include <vector>
#include <TLorentzVector.h>

#include <iostream>
#include <cstdio>

class MyxAODAnalysis_noChild : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis_noChild (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  bool SortTLorentz_AccPt(std::vector<TLorentzVector> &input_vec, std::vector<TLorentzVector> &sorted_vec);
  bool SortTLorentz_AccPt(std::vector<Lepton> &input_vec, std::vector<Lepton> &sorted_vec);

  double DeltaPhi(double phi1, double phi2) {
    double result = phi1 - phi2;
    while (result > M_PI)    result -= 2 * M_PI;
    while (result <= -M_PI)  result += 2 * M_PI;
    return std::abs(result);
  }

  double DeltaR(double eta1, double phi1, double eta2, double phi2) {
    double deta = eta1 - eta2;
    double dphi = DeltaPhi(phi1, phi2);
    return std::sqrt(deta*deta + dphi*dphi);
  }

  ~MyxAODAnalysis_noChild () override;

 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
  
  /// Electron pT cut
  double m_electronPtCut;
  /// Sample name
  std::string m_sampleName;

  unsigned int m_runNumber = 0; ///< Run number                                             
  unsigned long long m_eventNumber = 0; ///< Event number
  
  std::vector<float> *m_particleStatus = nullptr;
  std::vector<float> *m_particlePdgID =nullptr;
  std::vector<float> *m_particleEta = nullptr;
  std::vector<float> *m_particlePhi = nullptr;
  std::vector<float> *m_particlePt = nullptr;
  std::vector<float> *m_particleE = nullptr;
  std::vector<float> *m_particlePx = nullptr;
  std::vector<float> *m_particlePy = nullptr;
  std::vector<float> *m_particlePz = nullptr;

  double MZ_PDG = 91.1876;
  double MW_PDG = 83.385;
  double GammaZ_PDG = 2.4952;
  double GammaW_PDG = 2.085;
  //double M_PI = 3.14159;
};

#endif
