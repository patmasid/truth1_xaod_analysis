#include <MyAnalysis/Lepton.h>

void Lepton::Init(){
  m_PDGID = 0;
  m_ParticleStatus = -1;
  m_MaxEta = 1000;
  m_MinPt = 0.0;
  m_Pt = 0.0;
  m_Px = 0.0, m_Py = 0.0, m_Pz = 0.0, m_E = 0.0, m_M = 0.0;
}

void Lepton::set_PDGID(int PDGID){
  m_PDGID = PDGID;
}

void Lepton::set_ParticleStatus(int ParticleStatus){
  m_ParticleStatus = ParticleStatus;
}

void Lepton::set_TLorentzVector(double Px, double Py, double Pz, double E){
  m_TLorentzVector.SetPxPyPzE(Px, Py, Pz, E);
}

void Lepton::set_MaxEta(double Eta){
  m_MaxEta = Eta;
}

void Lepton::set_MinPt(double Pt){
  m_MinPt = Pt;
}

void Lepton::set_Px(double Px){
  m_Px = Px;
}

void Lepton::set_Py(double Py){
  m_Py = Py;
}

void Lepton::set_Pz(double Pz){
  m_Pz = Pz;
}

void Lepton::set_E(double E){
  m_E = E;
}

void Lepton::set_M(){
  m_M = m_TLorentzVector.M();
}

void Lepton::set_Pt(){
  m_Pt = m_TLorentzVector.Pt();
}

int Lepton::PDGID(){
  return m_PDGID;
}

int Lepton::ParticleStatus(){
  return m_ParticleStatus;
}

TLorentzVector Lepton::GetTLorentzVector(){
  return m_TLorentzVector;
}

double Lepton::MaxEta(){
  return m_MaxEta;
}

double Lepton::MinPt(){
  return m_MinPt;
}

double Lepton::M(){
  return m_M;
}

double Lepton::Px(){
  return m_Px;
}

double Lepton::Py(){
  return m_Py;
}

double Lepton::Pz(){
  return m_Pz;
}

double Lepton::E(){
  return m_E;
}

double Lepton::Pt(){
  return m_Pt;
}

double Lepton::Eta(){
  return GetTLorentzVector().Eta();
}

double Lepton::Phi(){
  return GetTLorentzVector().Phi();
}

double Lepton::Rho(){
  return GetTLorentzVector().Rho();
}

double Lepton::Theta(){
  return GetTLorentzVector().Theta();
}

double Lepton::CosTheta(){
  return GetTLorentzVector().CosTheta();
}
