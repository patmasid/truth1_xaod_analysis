#include <AsgTools/MessageCheck.h>
#include <xAODJet/JetContainer.h>
//#include <xAODMissingET/MissingETContainer.h>                                                                                                                                                             

//======== Adding TruthVertex headers ========//                                                                                                                                                            

#include <xAODTruth/TruthVertexContainer.h>

//============== For AnaToolHandle ===============//                                                                                                                                                        

#include <TSystem.h>

#include <TLorentzVector.h>
#include <MyAnalysis/MyxAODAnalysis_noChild.h>
#include <MyAnalysis/Lepton.h>
#include <xAODEventInfo/EventInfo.h>

#include <xAODTruth/TruthParticleContainer.h>
#include <TLorentzVector.h>
#include <cmath>
#include <vector>
#include <TH1.h>
#include "TF1.h"
#include <TVector3.h>

MyxAODAnalysis_noChild :: MyxAODAnalysis_noChild (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,"Minimum electron pT (in MeV)" );
  declareProperty( "SampleName", m_sampleName = "Unknown","Descriptive name for the processed sample" );

}

MyxAODAnalysis_noChild :: ~MyxAODAnalysis_noChild () {

  delete m_particleStatus;
  delete m_particlePdgID;
  delete m_particleEta;
  delete m_particlePhi;
  delete m_particlePt;
  delete m_particleE;
  delete m_particlePx;
  delete m_particlePy;
  delete m_particlePz;

}


StatusCode MyxAODAnalysis_noChild :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO ("in initialize");

  //============ Making N-tuples ========//                                                                                                           

  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);

  //=========== Particles                                                                                                                                       

  m_particleStatus = new std::vector<float>();
  m_particlePdgID = new std::vector<float>();
  m_particleEta = new std::vector<float>();
  m_particlePhi = new std::vector<float>();
  m_particlePt = new std::vector<float>();
  m_particleE = new std::vector<float>();
  m_particlePx = new std::vector<float>();
  m_particlePy = new std::vector<float>();
  m_particlePz = new std::vector<float>();

  mytree->Branch ("ParticleStatus", &m_particleStatus);
  mytree->Branch ("ParticlePdgID", &m_particlePdgID);
  mytree->Branch ("ParticleEta", &m_particleEta);
  mytree->Branch ("ParticlePhi", &m_particlePhi);
  mytree->Branch ("ParticlePt", &m_particlePt);
  mytree->Branch ("ParticleE", &m_particleE);
  mytree->Branch ("ParticlePx", &m_particlePx);
  mytree->Branch ("ParticlePy", &m_particlePy);
  mytree->Branch ("ParticlePz", &m_particlePz);

  //======================= Booking Histograms ============================//

  //ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]


  ANA_CHECK (book (TH1F("InvMass_W","InvMass_W",100,0,250)));
  ANA_CHECK (book (TH1F("InvMass_Z","InvMass_Z",100,0,250)));
  ANA_CHECK (book (TH1F("InvMass_WZ","InvMass_WZ",100,0,1000)));
  ANA_CHECK (book (TH1F("WZ_pT", "WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F("pT_W", "pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F("pT_Z", "pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F("CosTheta_W_Lab","CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_Z_Lab","CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_W_WZ","CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_Z_WZ","CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_W_WZ_WZdir","CosTheta_W_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_Z_WZ_WZdir","CosTheta_Z_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_WLep_Wrest","CosTheta_WLep_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_WLep_Wrest_wWZrest","CosTheta_WLep_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_ZLepM_Zrest","CosTheta_ZLepM_Zrest",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_ZLepM_Zrest_wWZrest","CosTheta_ZLepM_Zrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F("all_Lep_pT","all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F("all_Lep_Eta","all_Lep_Eta",50,-10,10)));
  ANA_CHECK (book (TH1F("all_Lep_Phi","all_Lep_Phi",50,-M_PI,M_PI)));
  ANA_CHECK (book (TH1F("W_Rapidity_Lab","W_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("W_Rapidity_WZ","W_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("W_Lepton_Rapidity_Lab","W_Lepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("W_Lepton_Rapidity_WZ","W_Lepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Z_Rapidity_Lab","Z_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Z_Rapidity_WZ","Z_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Z_NegLepton_Rapidity_Lab","Z_NegLepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Z_NegLepton_Rapidity_WZ","Z_NegLepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("DiffRapidity_W_Z_Lab","DiffRapidity_W_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("DiffRapidity_W_Z_WZ","DiffRapidity_W_Z_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("DiffRapidity_WLep_ZLepM_Lab","DiffRapidity_WLep_ZLepM_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("DiffRapidity_WLep_ZLepM_WZ","DiffRapidity_WLep_ZLepM_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("DiffRapidity_WLep_Z_Lab","DiffRapidity_WLep_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("DiffRapidity_WLep_Z_WZ","DiffRapidity_WLep_Z_WZ",100,-20,20)));

  ANA_CHECK (book (TH1F ("Cut1_WZ_pT", "Cut1_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut1_pT_W", "Cut1_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut1_pT_Z", "Cut1_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_W_Lab", "Cut1_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_Z_Lab", "Cut1_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_W_WZ", "Cut1_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_Z_WZ", "Cut1_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut1_CosTheta_W_WZ_WZdir","Cut1_CosTheta_W_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut1_CosTheta_Z_WZ_WZdir","Cut1_CosTheta_Z_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_WLep_Wrest", "Cut1_CosTheta_WLep_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_WLep_Wrest_wWZrest", "Cut1_CosTheta_WLep_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut1_CosTheta_ZLepM_Zrest","Cut1_CosTheta_ZLepM_Zrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut1_CosTheta_ZLepM_Zrest_wWZrest","Cut1_CosTheta_ZLepM_Zrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_all_Lep_pT", "Cut1_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut1_all_Lep_Eta", "Cut1_all_Lep_Eta",50,-10,10)));
  ANA_CHECK (book (TH1F("Cut1_all_Lep_Phi","Cut1_all_Lep_Phi",50,-M_PI,M_PI)));
  ANA_CHECK (book (TH1F("Cut1_W_Rapidity_Lab","Cut1_W_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_W_Rapidity_WZ","Cut1_W_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_W_Lepton_Rapidity_Lab","Cut1_W_Lepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_W_Lepton_Rapidity_WZ","Cut1_W_Lepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_Z_Rapidity_Lab","Cut1_Z_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_Z_Rapidity_WZ","Cut1_Z_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_Z_NegLepton_Rapidity_Lab","Cut1_Z_NegLepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_Z_NegLepton_Rapidity_WZ","Cut1_Z_NegLepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_DiffRapidity_W_Z_Lab","Cut1_DiffRapidity_W_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_DiffRapidity_W_Z_WZ","Cut1_DiffRapidity_W_Z_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_DiffRapidity_WLep_ZLepM_Lab","Cut1_DiffRapidity_WLep_ZLepM_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_DiffRapidity_WLep_ZLepM_WZ","Cut1_DiffRapidity_WLep_ZLepM_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_DiffRapidity_WLep_Z_Lab","Cut1_DiffRapidity_WLep_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut1_DiffRapidity_WLep_Z_WZ","Cut1_DiffRapidity_WLep_Z_WZ",100,-20,20)));

  ANA_CHECK (book (TH1F ("Cut2_WZ_pT", "Cut2_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut2_pT_W", "Cut2_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut2_pT_Z", "Cut2_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_W_Lab", "Cut2_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_Z_Lab", "Cut2_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_W_WZ", "Cut2_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_Z_WZ", "Cut2_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut2_CosTheta_W_WZ_WZdir","Cut2_CosTheta_W_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut2_CosTheta_Z_WZ_WZdir","Cut2_CosTheta_Z_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_WLep_Wrest", "Cut2_CosTheta_WLep_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_WLep_Wrest_wWZrest", "Cut2_CosTheta_WLep_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut2_CosTheta_ZLepM_Zrest","Cut2_CosTheta_ZLepM_Zrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut2_CosTheta_ZLepM_Zrest_wWZrest","Cut2_CosTheta_ZLepM_Zrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_all_Lep_pT", "Cut2_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut2_all_Lep_Eta", "Cut2_all_Lep_Eta",50,-10,10)));
  ANA_CHECK (book (TH1F("Cut2_all_Lep_Phi","Cut2_all_Lep_Phi",50,-M_PI,M_PI)));
  ANA_CHECK (book (TH1F("Cut2_W_Rapidity_Lab","Cut2_W_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_W_Rapidity_WZ","Cut2_W_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_W_Lepton_Rapidity_Lab","Cut2_W_Lepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_W_Lepton_Rapidity_WZ","Cut2_W_Lepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_Z_Rapidity_Lab","Cut2_Z_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_Z_Rapidity_WZ","Cut2_Z_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_Z_NegLepton_Rapidity_Lab","Cut2_Z_NegLepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_Z_NegLepton_Rapidity_WZ","Cut2_Z_NegLepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_DiffRapidity_W_Z_Lab","Cut2_DiffRapidity_W_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_DiffRapidity_W_Z_WZ","Cut2_DiffRapidity_W_Z_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_DiffRapidity_WLep_ZLepM_Lab","Cut2_DiffRapidity_WLep_ZLepM_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_DiffRapidity_WLep_ZLepM_WZ","Cut2_DiffRapidity_WLep_ZLepM_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_DiffRapidity_WLep_Z_Lab","Cut2_DiffRapidity_WLep_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut2_DiffRapidity_WLep_Z_WZ","Cut2_DiffRapidity_WLep_Z_WZ",100,-20,20)));
  
  ANA_CHECK (book (TH1F ("Cut3_WZ_pT", "Cut3_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut3_pT_W", "Cut3_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut3_pT_Z", "Cut3_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_W_Lab", "Cut3_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_Z_Lab", "Cut3_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_W_WZ", "Cut3_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_Z_WZ", "Cut3_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut3_CosTheta_W_WZ_WZdir","Cut3_CosTheta_W_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut3_CosTheta_Z_WZ_WZdir","Cut3_CosTheta_Z_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_WLep_Wrest", "Cut3_CosTheta_WLep_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_WLep_Wrest_wWZrest", "Cut3_CosTheta_WLep_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut3_CosTheta_ZLepM_Zrest","Cut3_CosTheta_ZLepM_Zrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut3_CosTheta_ZLepM_Zrest_wWZrest","Cut3_CosTheta_ZLepM_Zrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_all_Lep_pT", "Cut3_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut3_all_Lep_Eta", "Cut3_all_Lep_Eta",50,-10,10)));
  ANA_CHECK (book (TH1F("Cut3_all_Lep_Phi","Cut3_all_Lep_Phi",50,-M_PI,M_PI)));
  ANA_CHECK (book (TH1F("Cut3_W_Rapidity_Lab","Cut3_W_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_W_Rapidity_WZ","Cut3_W_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_W_Lepton_Rapidity_Lab","Cut3_W_Lepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_W_Lepton_Rapidity_WZ","Cut3_W_Lepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_Z_Rapidity_Lab","Cut3_Z_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_Z_Rapidity_WZ","Cut3_Z_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_Z_NegLepton_Rapidity_Lab","Cut3_Z_NegLepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_Z_NegLepton_Rapidity_WZ","Cut3_Z_NegLepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_DiffRapidity_W_Z_Lab","Cut3_DiffRapidity_W_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_DiffRapidity_W_Z_WZ","Cut3_DiffRapidity_W_Z_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_DiffRapidity_WLep_ZLepM_Lab","Cut3_DiffRapidity_WLep_ZLepM_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_DiffRapidity_WLep_ZLepM_WZ","Cut3_DiffRapidity_WLep_ZLepM_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_DiffRapidity_WLep_Z_Lab","Cut3_DiffRapidity_WLep_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut3_DiffRapidity_WLep_Z_WZ","Cut3_DiffRapidity_WLep_Z_WZ",100,-20,20)));

  ANA_CHECK (book (TH1F ("Cut4_WZ_pT", "Cut4_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut4_pT_W", "Cut4_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut4_pT_Z", "Cut4_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_W_Lab", "Cut4_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_Z_Lab", "Cut4_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_W_WZ", "Cut4_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_Z_WZ", "Cut4_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut4_CosTheta_W_WZ_WZdir","Cut4_CosTheta_W_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut4_CosTheta_Z_WZ_WZdir","Cut4_CosTheta_Z_WZ_WZdir",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_WLep_Wrest", "Cut4_CosTheta_WLep_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_WLep_Wrest_wWZrest", "Cut4_CosTheta_WLep_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut4_CosTheta_ZLepM_Zrest","Cut4_CosTheta_ZLepM_Zrest",20,-1,1)));
  ANA_CHECK (book (TH1F("Cut4_CosTheta_ZLepM_Zrest_wWZrest","Cut4_CosTheta_ZLepM_Zrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_all_Lep_pT", "Cut4_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut4_all_Lep_Eta", "Cut4_all_Lep_Eta",50,-10,10)));
  ANA_CHECK (book (TH1F("Cut4_all_Lep_Phi","Cut4_all_Lep_Phi",50,-M_PI,M_PI)));
  ANA_CHECK (book (TH1F("Cut4_W_Rapidity_Lab","Cut4_W_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_W_Rapidity_WZ","Cut4_W_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_W_Lepton_Rapidity_Lab","Cut4_W_Lepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_W_Lepton_Rapidity_WZ","Cut4_W_Lepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_Z_Rapidity_Lab","Cut4_Z_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_Z_Rapidity_WZ","Cut4_Z_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_Z_NegLepton_Rapidity_Lab","Cut4_Z_NegLepton_Rapidity_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_Z_NegLepton_Rapidity_WZ","Cut4_Z_NegLepton_Rapidity_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_DiffRapidity_W_Z_Lab","Cut4_DiffRapidity_W_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_DiffRapidity_W_Z_WZ","Cut4_DiffRapidity_W_Z_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_DiffRapidity_WLep_ZLepM_Lab","Cut4_DiffRapidity_WLep_ZLepM_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_DiffRapidity_WLep_ZLepM_WZ","Cut4_DiffRapidity_WLep_ZLepM_WZ",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_DiffRapidity_WLep_Z_Lab","Cut4_DiffRapidity_WLep_Z_Lab",100,-20,20)));
  ANA_CHECK (book (TH1F("Cut4_DiffRapidity_WLep_Z_WZ","Cut4_DiffRapidity_WLep_Z_WZ",100,-20,20)));

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis_noChild :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //ANA_MSG_INFO ("in execute");

  //============Using TruthParticles ===============//                                                                                                         

  // Read/fill the EventInfo variables:                                                                                                                              
  const xAOD::EventInfo* ei = nullptr;
  ANA_CHECK (evtStore()->retrieve (ei, "EventInfo"));
  m_runNumber = ei->runNumber ();
  m_eventNumber = ei->eventNumber ();

  m_particleStatus->clear();
  m_particlePdgID->clear();
  m_particleEta->clear();
  m_particlePhi->clear();
  m_particlePt->clear();
  m_particleE->clear();
  m_particlePx->clear();
  m_particlePy->clear();
  m_particlePz->clear();

  // Read/fill the electron variables:                                                                                                                                                  

  int num_W=0, num_Z=0;

  const xAOD::TruthParticleContainer* particles = nullptr;
  ANA_CHECK (evtStore()->retrieve (particles, "TruthParticles"));
  for (const xAOD::TruthParticle* particle : *particles) {
    if(abs(particle->pdgId())>=11 && abs(particle->pdgId())<=14 && (particle->pt())*0.001 > 15){
      //ANA_MSG_INFO ("Particle Info: Particle PID- " << particle->pdgId() << " Particle Status- " << particle->status() << " Particle Pt- " << (particle->pt())*0.001);
    }
    if(abs(particle->pdgId())==24 && particle->status()==22) num_W+=1;
    if(abs(particle->pdgId())==23 && particle->status()==22) num_Z+=1;
  }

  const xAOD::TruthParticle *particle_Child_1;
  const xAOD::TruthParticle *particle_Child_2;
  const xAOD::TruthParticle *particle_Child_3;
  const xAOD::TruthParticle *particle_Child_4;

  TLorentzVector truthWLep, truthWNeu, truthZLepM, truthZLepP, truthW, truthZ, truthWZ;
  int truthWLep_pdgid, truthWNeu_pdgid, truthZLepM_pdgid, truthZLepP_pdgid, truthW_pdgid, truthZ_pdgid;

  std::vector<TLorentzVector> v_inputLeptonsTLorentz, v_SortedLeptonsTLorentz;
  std::vector<Lepton> v_inputLeptons, v_sortedLeptons;
  std::vector<Lepton> v_inputNeutrinos, v_sortedNeutrinos;

  for (const xAOD::TruthParticle* particle : *particles) { 
    if(abs(particle->pdgId())>=11 && abs(particle->pdgId())<=14 && (particle->pt())*0.001 > 15 && particle->eta()<2.5){
      TLorentzVector currVector;
      m_particleStatus->push_back (particle->status());
      m_particlePdgID->push_back(particle->pdgId());
      m_particleEta->push_back (particle->eta ());
      m_particlePhi->push_back (particle->phi ());
      m_particlePt-> push_back ((particle->pt ())*0.001);
      m_particleE->  push_back ((particle->e ())*0.001);
      m_particlePx-> push_back ((particle->px ())*0.001);
      m_particlePy-> push_back ((particle->py ())*0.001);
      m_particlePz-> push_back ((particle->pz ())*0.001);
      currVector.SetPxPyPzE(particle->px()*0.001, particle->py()*0.001, particle->pz()*0.001, particle->e()*0.001);
      v_inputLeptonsTLorentz.push_back(currVector);
      
      if( abs(particle->pdgId())==11 || abs(particle->pdgId())==13 ){
	Lepton currLepton(particle->pdgId(), particle->status(), (particle->e ())*0.001, (particle->px ())*0.001, (particle->py ())*0.001, (particle->pz ())*0.001, 2.5, 15);
	v_inputLeptons.push_back(currLepton);
      }
      else if( abs(particle->pdgId())==12 || abs(particle->pdgId())==14 ) {
	Lepton currNeutrino(particle->pdgId(), particle->status(), (particle->e ())*0.001, (particle->px ())*0.001, (particle->py ())*0.001, (particle->pz ())*0.001, 2.5, 15);
	v_inputNeutrinos.push_back(currNeutrino);
	//ANA_MSG_INFO ("Particle Pt from input vector neutrino: " << particle->pt ()*0.001); 
      }
      //ANA_MSG_INFO ("Particle Pt from input vector: " << particle->pt ()*0.001);
    }
  }

  tree ("analysis")->Fill ();
  
  if( !(v_inputLeptons.size() < 3 || v_inputNeutrinos.size() < 1) ) {

    for(int ii=0; ii<v_inputNeutrinos.size(); ii++){
      //ANA_MSG_INFO (ii << " Pt of neutrino: " << v_inputNeutrinos[ii].Pt());
    }

    if(!SortTLorentz_AccPt(v_inputLeptons, v_sortedLeptons)){
      //ANA_MSG_ERROR ("Cannot sort Leptons.");
    }
    
    for(int ii=0; ii<v_sortedLeptons.size(); ++ii){
      //ANA_MSG_INFO (ii << " Pt of Lepton: " << v_sortedLeptons[ii].Pt());
    }
    
    if(!SortTLorentz_AccPt(v_inputNeutrinos, v_sortedNeutrinos)){
      //ANA_MSG_INFO ("Cannot sort Neutrinos. ");
    }
    
    for(int ii=0; ii<v_sortedNeutrinos.size(); ++ii){
      //ANA_MSG_INFO (ii << " Pt of neutrino: " << v_sortedNeutrinos[ii].Pt());
    }
    
    bool foundWandZ = false;
    
    int i, j, k;
    double MassZ01 = 0., MassZ02 = 0., MassZ12 = 0.;
    double MassW0 = 0., MassW1 = 0., MassW2 = 0.;
    double WeightZ1, WeightZ2, WeightZ3;
    double WeightW1, WeightW2, WeightW3;
    double M1, M2, M3;
    double WeightTotal1, WeightTotal2, WeightTotal3;
    double W0_pt, Z12_pt, W1_pt, Z02_pt, W2_pt, Z01_pt, WZ_pt;

    //MSG_INFO("try Z pair of leptons 01");                                                                                                  
    if ( (v_sortedLeptons[0].PDGID() ==-(v_sortedLeptons[1].PDGID())) && (v_sortedLeptons[2].PDGID()==(-1)*v_sortedNeutrinos[0].PDGID()*(abs(v_sortedNeutrinos[0].PDGID())-1)/abs(v_sortedNeutrinos[0].PDGID()) ) ){
      MassZ01 = (v_sortedLeptons[0].GetTLorentzVector()+v_sortedLeptons[1].GetTLorentzVector()).M();
      MassW2 = (v_sortedLeptons[2].GetTLorentzVector()+v_sortedNeutrinos[0].GetTLorentzVector()).M();
      Z01_pt = (v_sortedLeptons[0].GetTLorentzVector()+v_sortedLeptons[1].GetTLorentzVector()).Pt();
      W2_pt = (v_sortedLeptons[2].GetTLorentzVector()+v_sortedNeutrinos[0].GetTLorentzVector()).Pt();
    }
    //try Z pair of leptons 02                                                                                                               
    if ( (v_sortedLeptons[0].PDGID() ==-(v_sortedLeptons[2].PDGID())) && (v_sortedLeptons[1].PDGID()==(-1)*v_sortedNeutrinos[0].PDGID()*(abs(v_sortedNeutrinos[0].PDGID())-1)/abs(v_sortedNeutrinos[0].PDGID()) ) ){
      MassZ02 = (v_sortedLeptons[0].GetTLorentzVector()+v_sortedLeptons[2].GetTLorentzVector()).M();
      MassW1 = (v_sortedLeptons[1].GetTLorentzVector()+v_sortedNeutrinos[0].GetTLorentzVector()).M();
      Z02_pt = (v_sortedLeptons[0].GetTLorentzVector()+v_sortedLeptons[2].GetTLorentzVector()).Pt();
      W1_pt = (v_sortedLeptons[1].GetTLorentzVector()+v_sortedNeutrinos[0].GetTLorentzVector()).Pt();
    }
    //try Z pair of leptons 12                                                                                                               
    if ( (v_sortedLeptons[1].PDGID() ==-(v_sortedLeptons[2].PDGID())) && (v_sortedLeptons[0].PDGID()==(-1)*v_sortedNeutrinos[0].PDGID()*(abs(v_sortedNeutrinos[0].PDGID())-1)/abs(v_sortedNeutrinos[0].PDGID()) ) ){
      MassZ12 = (v_sortedLeptons[1].GetTLorentzVector()+v_sortedLeptons[2].GetTLorentzVector()).M();
      MassW0 = (v_sortedLeptons[0].GetTLorentzVector()+v_sortedNeutrinos[0].GetTLorentzVector()).M();
      Z12_pt = (v_sortedLeptons[1].GetTLorentzVector()+v_sortedLeptons[2].GetTLorentzVector()).Pt();
      W0_pt = (v_sortedLeptons[0].GetTLorentzVector()+v_sortedNeutrinos[0].GetTLorentzVector()).Pt();
    }
    
    WeightZ1 = 1/(pow(MassZ01*MassZ01 - MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
    WeightW1 = 1/(pow(MassW2*MassW2 - MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
    WeightTotal1 = WeightZ1*WeightW1;
    M1 = -1*WeightTotal1;

    WeightZ2 = 1/(pow(MassZ02*MassZ02- MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));
    WeightW2 = 1/(pow(MassW1*MassW1- MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
    WeightTotal2 = WeightZ2*WeightW2;
    M2 = -1*WeightTotal2;

    WeightZ3 = 1/(pow(MassZ12*MassZ12 - MZ_PDG*MZ_PDG,2) + pow(MZ_PDG*GammaZ_PDG,2));

    WeightW3 = 1/(pow(MassW0*MassW0 - MW_PDG*MW_PDG,2) + pow(MW_PDG*GammaW_PDG,2));
    WeightTotal3 = WeightZ3*WeightW3;
    M3 = -1*WeightTotal3;
    
    if( (M1 < M2 && M1 < M3) || (MassZ01 != 0 && MassW2 != 0 && MassZ02 == 0 && MassZ12 == 0) ){
      i = 0; j = 1; k = 2;
    }
    if((M2 < M1 && M2 < M3) || (MassZ02 != 0 && MassW1 != 0 && MassZ01 == 0 && MassZ12 == 0) ){
      i = 0; j = 2; k = 1;
    }
    if((M3 < M1 && M3 < M2) || (MassZ12 != 0 && MassW0 != 0 && MassZ01 == 0 && MassZ02 == 0) ){
      i = 1; j = 2; k = 0;
    }

    if( (v_sortedLeptons[i].PDGID() ==-(v_sortedLeptons[j].PDGID())) && (v_sortedLeptons[k].PDGID()==(-1)*v_sortedNeutrinos[0].PDGID()*(abs(v_sortedNeutrinos[0].PDGID())-1)/abs(v_sortedNeutrinos[0].PDGID()) ) ) {
      foundWandZ = true;
    }

    if( foundWandZ==true ) {
      
      ANA_MSG_INFO ("execute(): ZLepP Px Py Pz "<<truthZLepP.Px()<<" "<<truthZLepP.Py()<<" "<<truthZLepP.Pz());
      
      truthWLep = v_sortedLeptons[k].GetTLorentzVector();
      truthWNeu = v_sortedNeutrinos[0].GetTLorentzVector();
      if( v_sortedLeptons[i].PDGID() < 0 ){
	truthZLepP = v_sortedLeptons[j].GetTLorentzVector();
	truthZLepM = v_sortedLeptons[i].GetTLorentzVector();
      }
      else if( v_sortedLeptons[i].PDGID() > 0 ){
	truthZLepP = v_sortedLeptons[i].GetTLorentzVector();
	truthZLepM = v_sortedLeptons[j].GetTLorentzVector();
      }
      truthW = v_sortedLeptons[k].GetTLorentzVector() + v_sortedNeutrinos[0].GetTLorentzVector();
      truthZ = v_sortedLeptons[i].GetTLorentzVector() + v_sortedLeptons[j].GetTLorentzVector();
      truthWZ = truthW + truthZ;
      
      TLorentzVector truthWLep_WZ, truthWNeu_WZ, truthZLepM_WZ, truthZLepP_WZ, truthW_WZ, truthZ_WZ;
      
      truthWLep_WZ = truthWLep;
      truthWLep_WZ.Boost((-1)*truthWZ.BoostVector());
      
      truthWNeu_WZ = truthWNeu;
      truthWNeu_WZ.Boost((-1)*truthWZ.BoostVector());
      
      truthZLepM_WZ = truthZLepM;
      truthZLepM_WZ.Boost((-1)*truthWZ.BoostVector());
      
      truthZLepP_WZ = truthZLepP;
      truthZLepP_WZ.Boost((-1)*truthWZ.BoostVector());
      
      truthW_WZ = truthW;
      truthW_WZ.Boost((-1)*truthWZ.BoostVector());
      
      truthZ_WZ = truthZ;
      truthZ_WZ.Boost((-1)*truthWZ.BoostVector());
      
      double CosAngle_LepWrest_W, CosAngle_LepWrest_W_WZ, CosAngle_LepMZrest_Z, CosAngle_LepMZrest_Z_WZ, CosAngle_W_WZ_WZdir, CosAngle_Z_WZ_WZdir;
      
      CosAngle_W_WZ_WZdir = cos((truthW_WZ.Vect()).Angle(truthWZ.Vect()));
      CosAngle_Z_WZ_WZdir = cos((truthZ_WZ.Vect()).Angle(truthWZ.Vect()));

      TLorentzVector Lep_WFrame = truthWLep;
      Lep_WFrame.Boost((-1)*truthW.BoostVector());
      CosAngle_LepWrest_W=cos((Lep_WFrame.Vect()).Angle(truthW.Vect()));
      
      TLorentzVector Lep_WFrame_WZ = truthWLep_WZ;
      Lep_WFrame_WZ.Boost((-1)*truthW_WZ.BoostVector());
      CosAngle_LepWrest_W_WZ=cos((Lep_WFrame_WZ.Vect()).Angle(truthW_WZ.Vect()));
      
      TLorentzVector LepM_ZFrame = truthZLepM;
      LepM_ZFrame.Boost((-1)*truthZ.BoostVector());
      CosAngle_LepMZrest_Z=cos((LepM_ZFrame.Vect()).Angle(truthZ.Vect()));

      TLorentzVector LepM_ZFrame_WZ = truthZLepM_WZ;
      LepM_ZFrame_WZ.Boost((-1)*truthZ_WZ.BoostVector());
      CosAngle_LepMZrest_Z_WZ=cos((LepM_ZFrame_WZ.Vect()).Angle(truthZ_WZ.Vect()));

      //tree ("analysis")->Fill ();
      
      //================== Some kinematic variables ================//
      
      double dphi_Wlep_Wneu = DeltaPhi(truthWLep.Phi(), truthWNeu.Phi());
      
      double W_mT = sqrt( 2 * truthWLep.Pt() * truthWNeu.Pt() * (1 - cos(dphi_Wlep_Wneu)) );
      
      double dR_Zlep1_Zlep2 = DeltaR(truthZLepP.Eta(), truthZLepP.Phi(), truthZLepM.Eta(), truthZLepM.Phi());
      double dR_Zlep1_Wlep = DeltaR(truthZLepP.Eta(), truthZLepP.Phi(), truthWLep.Eta(), truthWLep.Phi());
      double dR_Zlep2_Wlep = DeltaR(truthZLepM.Eta(), truthZLepM.Phi(), truthWLep.Eta(), truthWLep.Phi());
      
      Double_t W_Rapidity_Lab= truthW.Rapidity();
      Double_t W_Rapidity_WZ= truthW_WZ.Rapidity();

      Double_t W_Lepton_Rapidity_Lab = truthWLep.Rapidity();
      Double_t W_Lepton_Rapidity_WZ = truthWLep_WZ.Rapidity();
      
      Double_t Z_Rapidity_Lab= truthZ.Rapidity();
      Double_t Z_Rapidity_WZ= truthZ_WZ.Rapidity();

      Double_t Z_NegLepton_Rapidity_Lab = truthZLepM.Rapidity();
      Double_t Z_NegLepton_Rapidity_WZ = truthZLepM_WZ.Rapidity();
      
      double DiffRapidity_W_Z_Lab = W_Rapidity_Lab-Z_Rapidity_Lab;
      double DiffRapidity_W_Z_WZ = W_Rapidity_WZ-Z_Rapidity_WZ;

      double DiffRapidity_WLep_ZLepM_Lab = W_Lepton_Rapidity_Lab-Z_NegLepton_Rapidity_Lab;
      double DiffRapidity_WLep_ZLepM_WZ = W_Lepton_Rapidity_WZ-Z_NegLepton_Rapidity_WZ;

      double DiffRapidity_WLep_Z_Lab = W_Lepton_Rapidity_Lab-Z_Rapidity_Lab;
      double DiffRapidity_WLep_Z_WZ = W_Lepton_Rapidity_WZ-Z_Rapidity_WZ;

      //================================== Filling Histograms ============================================//

      ANA_MSG_INFO ("execute(): truthZ.M() " << truthZ.M());
      
      if ( !(fabs(truthZ.M() - MZ_PDG) >= 10. ) ){//|| W_mT <= 30 || dR_Zlep1_Zlep2 < 0.2 || dR_Zlep1_Wlep < 0.3 || dR_Zlep2_Wlep < 0.3) ){
	
	//ANA_MSG_INFO ("execute():Children pdgid "<<truthWLep_pdgid<<" "<<truthWNeu_pdgid<<" "<<truthZLepM_pdgid<<" "<<truthZLepP_pdgid);
	
	//if( !( abs(truthWLep_pdgid)==15 || abs(truthWNeu_pdgid)==16 || abs(truthZLepM_pdgid)==15 || abs(truthZLepP_pdgid)==15 ) ){
	
	//if( truthWLep.Pt()>15 && truthZLepP.Pt()>15 && truthZLepM.Pt()>15 && std::abs(truthWLep.Eta())<2.5 && abs(truthZLepP.Eta())<2.5 && abs(truthZLepM.Eta())<2.5){
	
	hist ("InvMass_W")->Fill (truthW.M());
	hist ("InvMass_Z")->Fill (truthZ.M());
	hist ("InvMass_WZ")->Fill (truthWZ.M());
	hist ("WZ_pT")->Fill ((truthW+truthZ).Pt());
	hist ("pT_W")->Fill (truthW.Pt());
	hist ("pT_Z")->Fill (truthZ.Pt());
	hist ("CosTheta_W_Lab")->Fill (truthW.CosTheta());
	hist ("CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	hist ("CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	hist ("CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	hist ("CosTheta_W_WZ_WZdir")->Fill (CosAngle_W_WZ_WZdir);
        hist ("CosTheta_Z_WZ_WZdir")->Fill (CosAngle_Z_WZ_WZdir);
	hist ("CosTheta_WLep_Wrest")->Fill (CosAngle_LepWrest_W);
	hist ("CosTheta_WLep_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	hist ("CosTheta_ZLepM_Zrest")->Fill (CosAngle_LepMZrest_Z);
        hist ("CosTheta_ZLepM_Zrest_wWZrest")->Fill (CosAngle_LepMZrest_Z_WZ);
	hist ("all_Lep_pT")->Fill (truthWLep.Pt());
	hist ("all_Lep_pT")->Fill (truthZLepM.Pt());
	hist ("all_Lep_pT")->Fill (truthZLepP.Pt());
	hist ("all_Lep_Eta")->Fill (truthWLep.Eta());
	hist ("all_Lep_Eta")->Fill (truthZLepM.Eta());
	hist ("all_Lep_Eta")->Fill (truthZLepP.Eta());
	hist ("all_Lep_Phi")->Fill (truthWLep.Phi());
        hist ("all_Lep_Phi")->Fill (truthZLepM.Phi());
        hist ("all_Lep_Phi")->Fill (truthZLepP.Phi());
	hist ("W_Rapidity_Lab")->Fill (W_Rapidity_Lab);
	hist ("W_Rapidity_WZ")->Fill (W_Rapidity_WZ);
	hist ("Z_Rapidity_Lab")->Fill (Z_Rapidity_Lab);
	hist ("Z_Rapidity_WZ")->Fill (Z_Rapidity_WZ);
	hist ("W_Lepton_Rapidity_Lab")->Fill (W_Lepton_Rapidity_Lab);
	hist ("W_Lepton_Rapidity_WZ")->Fill (W_Lepton_Rapidity_WZ);
	hist ("Z_NegLepton_Rapidity_Lab")->Fill (Z_NegLepton_Rapidity_Lab);
	hist ("Z_NegLepton_Rapidity_WZ")->Fill (Z_NegLepton_Rapidity_WZ);
	hist ("DiffRapidity_W_Z_Lab")->Fill (DiffRapidity_W_Z_Lab);
	hist ("DiffRapidity_W_Z_WZ")->Fill (DiffRapidity_W_Z_WZ);
	hist ("DiffRapidity_WLep_ZLepM_Lab")->Fill (DiffRapidity_WLep_ZLepM_Lab);
	hist ("DiffRapidity_WLep_ZLepM_WZ")->Fill (DiffRapidity_WLep_ZLepM_WZ);
	hist ("DiffRapidity_WLep_Z_Lab")->Fill (DiffRapidity_WLep_Z_Lab);
	hist ("DiffRapidity_WLep_Z_WZ")->Fill (DiffRapidity_WLep_Z_WZ);

	//    ANA_MSG_INFO ("execute(): truthWLep.Pt() truthZLepP.Pt() truthZLepM.Pt() "<<truthWLep.Pt()<<" "<<truthZLepP.Pt()<<" "<<truthZLepM.Pt());
	
	if( truthWLep.Pt()>25 && truthZLepP.Pt()>25 && truthZLepM.Pt()>25 && truthWNeu.Pt()>25 && abs(truthWLep.Eta())<2.5 && abs(truthZLepP.Eta())<2.5 && abs(truthZLepM.Eta())<2.5 ){
	  
	  //ANA_MSG_INFO ("execute(): Woahhhhhhhhh ");
	  
	  hist ("Cut1_WZ_pT")->Fill ((truthW+truthZ).Pt());
	  hist ("Cut1_pT_W")->Fill (truthW.Pt());
	  hist ("Cut1_pT_Z")->Fill (truthZ.Pt());
	  hist ("Cut1_CosTheta_W_Lab")->Fill (truthW.CosTheta());
	  hist ("Cut1_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	  hist ("Cut1_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	  hist ("Cut1_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	  hist ("Cut1_CosTheta_W_WZ_WZdir")->Fill (CosAngle_W_WZ_WZdir);
          hist ("Cut1_CosTheta_Z_WZ_WZdir")->Fill (CosAngle_Z_WZ_WZdir);
	  hist ("Cut1_CosTheta_WLep_Wrest")->Fill (CosAngle_LepWrest_W);
	  hist ("Cut1_CosTheta_WLep_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	  hist ("Cut1_CosTheta_ZLepM_Zrest")->Fill (CosAngle_LepMZrest_Z);
	  hist ("Cut1_CosTheta_ZLepM_Zrest_wWZrest")->Fill (CosAngle_LepMZrest_Z_WZ);
	  hist ("Cut1_all_Lep_pT")->Fill (truthWLep.Pt());
	  hist ("Cut1_all_Lep_pT")->Fill (truthZLepM.Pt());
	  hist ("Cut1_all_Lep_pT")->Fill (truthZLepP.Pt());
	  hist ("Cut1_all_Lep_Eta")->Fill (truthWLep.Eta());
	  hist ("Cut1_all_Lep_Eta")->Fill (truthZLepM.Eta());
	  hist ("Cut1_all_Lep_Eta")->Fill (truthZLepP.Eta());
	  hist ("Cut1_all_Lep_Phi")->Fill (truthWLep.Phi());
	  hist ("Cut1_all_Lep_Phi")->Fill (truthZLepM.Phi());
	  hist ("Cut1_all_Lep_Phi")->Fill (truthZLepP.Phi());
	  hist ("Cut1_W_Rapidity_Lab")->Fill (W_Rapidity_Lab);
	  hist ("Cut1_W_Rapidity_WZ")->Fill (W_Rapidity_WZ);
	  hist ("Cut1_Z_Rapidity_Lab")->Fill (Z_Rapidity_Lab);
	  hist ("Cut1_Z_Rapidity_WZ")->Fill (Z_Rapidity_WZ);
	  hist ("Cut1_W_Lepton_Rapidity_Lab")->Fill (W_Lepton_Rapidity_Lab);
	  hist ("Cut1_W_Lepton_Rapidity_WZ")->Fill (W_Lepton_Rapidity_WZ);
	  hist ("Cut1_Z_NegLepton_Rapidity_Lab")->Fill (Z_NegLepton_Rapidity_Lab);
	  hist ("Cut1_Z_NegLepton_Rapidity_WZ")->Fill (Z_NegLepton_Rapidity_WZ);
	  hist ("Cut1_DiffRapidity_W_Z_Lab")->Fill (DiffRapidity_W_Z_Lab);
	  hist ("Cut1_DiffRapidity_W_Z_WZ")->Fill (DiffRapidity_W_Z_WZ);
	  hist ("Cut1_DiffRapidity_WLep_ZLepM_Lab")->Fill (DiffRapidity_WLep_ZLepM_Lab);
	  hist ("Cut1_DiffRapidity_WLep_ZLepM_WZ")->Fill (DiffRapidity_WLep_ZLepM_WZ);
	  hist ("Cut1_DiffRapidity_WLep_Z_Lab")->Fill (DiffRapidity_WLep_Z_Lab);
	  hist ("Cut1_DiffRapidity_WLep_Z_WZ")->Fill (DiffRapidity_WLep_Z_WZ);

	  if( truthZ.Pt()>200 ){
	    
	    hist ("Cut2_WZ_pT")->Fill ((truthW+truthZ).Pt());
	    hist ("Cut2_pT_W")->Fill (truthW.Pt());
	    hist ("Cut2_pT_Z")->Fill (truthZ.Pt());
	    hist ("Cut2_CosTheta_W_Lab")->Fill (truthW.CosTheta());
	    hist ("Cut2_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	    hist ("Cut2_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	    hist ("Cut2_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	    hist ("Cut2_CosTheta_W_WZ_WZdir")->Fill (CosAngle_W_WZ_WZdir);
            hist ("Cut2_CosTheta_Z_WZ_WZdir")->Fill (CosAngle_Z_WZ_WZdir);
	    hist ("Cut2_CosTheta_WLep_Wrest")->Fill (CosAngle_LepWrest_W);
	    hist ("Cut2_CosTheta_WLep_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	    hist ("Cut2_CosTheta_ZLepM_Zrest")->Fill (CosAngle_LepMZrest_Z);
	    hist ("Cut2_CosTheta_ZLepM_Zrest_wWZrest")->Fill (CosAngle_LepMZrest_Z_WZ);
	    hist ("Cut2_all_Lep_pT")->Fill (truthWLep.Pt());
	    hist ("Cut2_all_Lep_pT")->Fill (truthZLepM.Pt());
	    hist ("Cut2_all_Lep_pT")->Fill (truthZLepP.Pt());
	    hist ("Cut2_all_Lep_Eta")->Fill (truthWLep.Eta());
	    hist ("Cut2_all_Lep_Eta")->Fill (truthZLepM.Eta());
	    hist ("Cut2_all_Lep_Eta")->Fill (truthZLepP.Eta());
	    hist ("Cut2_all_Lep_Phi")->Fill (truthWLep.Phi());
	    hist ("Cut2_all_Lep_Phi")->Fill (truthZLepM.Phi());
	    hist ("Cut2_all_Lep_Phi")->Fill (truthZLepP.Phi());
	    hist ("Cut2_W_Rapidity_Lab")->Fill (W_Rapidity_Lab);
	    hist ("Cut2_W_Rapidity_WZ")->Fill (W_Rapidity_WZ);
	    hist ("Cut2_Z_Rapidity_Lab")->Fill (Z_Rapidity_Lab);
	    hist ("Cut2_Z_Rapidity_WZ")->Fill (Z_Rapidity_WZ);
	    hist ("Cut2_W_Lepton_Rapidity_Lab")->Fill (W_Lepton_Rapidity_Lab);
	    hist ("Cut2_W_Lepton_Rapidity_WZ")->Fill (W_Lepton_Rapidity_WZ);
	    hist ("Cut2_Z_NegLepton_Rapidity_Lab")->Fill (Z_NegLepton_Rapidity_Lab);
	    hist ("Cut2_Z_NegLepton_Rapidity_WZ")->Fill (Z_NegLepton_Rapidity_WZ);
	    hist ("Cut2_DiffRapidity_W_Z_Lab")->Fill (DiffRapidity_W_Z_Lab);
	    hist ("Cut2_DiffRapidity_W_Z_WZ")->Fill (DiffRapidity_W_Z_WZ);
	    hist ("Cut2_DiffRapidity_WLep_ZLepM_Lab")->Fill (DiffRapidity_WLep_ZLepM_Lab);
	    hist ("Cut2_DiffRapidity_WLep_ZLepM_WZ")->Fill (DiffRapidity_WLep_ZLepM_WZ);
	    hist ("Cut2_DiffRapidity_WLep_Z_Lab")->Fill (DiffRapidity_WLep_Z_Lab);
	    hist ("Cut2_DiffRapidity_WLep_Z_WZ")->Fill (DiffRapidity_WLep_Z_WZ);

	    if( truthWZ.Pt()<70 ){
	      
	      hist ("Cut3_WZ_pT")->Fill ((truthW+truthZ).Pt());
	      hist ("Cut3_pT_W")->Fill (truthW.Pt());
	      hist ("Cut3_pT_Z")->Fill (truthZ.Pt());
	      hist ("Cut3_CosTheta_W_Lab")->Fill (truthW.CosTheta());
	      hist ("Cut3_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	      hist ("Cut3_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	      hist ("Cut3_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	      hist ("Cut3_CosTheta_W_WZ_WZdir")->Fill (CosAngle_W_WZ_WZdir);
              hist ("Cut3_CosTheta_Z_WZ_WZdir")->Fill (CosAngle_Z_WZ_WZdir);
	      hist ("Cut3_CosTheta_WLep_Wrest")->Fill (CosAngle_LepWrest_W);
	      hist ("Cut3_CosTheta_WLep_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	      hist ("Cut3_CosTheta_ZLepM_Zrest")->Fill (CosAngle_LepMZrest_Z);
	      hist ("Cut3_CosTheta_ZLepM_Zrest_wWZrest")->Fill (CosAngle_LepMZrest_Z_WZ);
	      hist ("Cut3_all_Lep_pT")->Fill (truthWLep.Pt());
	      hist ("Cut3_all_Lep_pT")->Fill (truthZLepM.Pt());
	      hist ("Cut3_all_Lep_pT")->Fill (truthZLepP.Pt());
	      hist ("Cut3_all_Lep_Eta")->Fill (truthWLep.Eta());
	      hist ("Cut3_all_Lep_Eta")->Fill (truthZLepM.Eta());
	      hist ("Cut3_all_Lep_Eta")->Fill (truthZLepP.Eta());
	      hist ("Cut3_all_Lep_Phi")->Fill (truthWLep.Phi());
	      hist ("Cut3_all_Lep_Phi")->Fill (truthZLepM.Phi());
	      hist ("Cut3_all_Lep_Phi")->Fill (truthZLepP.Phi());
	      hist ("Cut3_W_Rapidity_Lab")->Fill (W_Rapidity_Lab);
	      hist ("Cut3_W_Rapidity_WZ")->Fill (W_Rapidity_WZ);
	      hist ("Cut3_Z_Rapidity_Lab")->Fill (Z_Rapidity_Lab);
	      hist ("Cut3_Z_Rapidity_WZ")->Fill (Z_Rapidity_WZ);
	      hist ("Cut3_W_Lepton_Rapidity_Lab")->Fill (W_Lepton_Rapidity_Lab);
	      hist ("Cut3_W_Lepton_Rapidity_WZ")->Fill (W_Lepton_Rapidity_WZ);
	      hist ("Cut3_Z_NegLepton_Rapidity_Lab")->Fill (Z_NegLepton_Rapidity_Lab);
	      hist ("Cut3_Z_NegLepton_Rapidity_WZ")->Fill (Z_NegLepton_Rapidity_WZ);
	      hist ("Cut3_DiffRapidity_W_Z_Lab")->Fill (DiffRapidity_W_Z_Lab);
	      hist ("Cut3_DiffRapidity_W_Z_WZ")->Fill (DiffRapidity_W_Z_WZ);
	      hist ("Cut3_DiffRapidity_WLep_ZLepM_Lab")->Fill (DiffRapidity_WLep_ZLepM_Lab);
	      hist ("Cut3_DiffRapidity_WLep_ZLepM_WZ")->Fill (DiffRapidity_WLep_ZLepM_WZ);
	      hist ("Cut3_DiffRapidity_WLep_Z_Lab")->Fill (DiffRapidity_WLep_Z_Lab);
	      hist ("Cut3_DiffRapidity_WLep_Z_WZ")->Fill (DiffRapidity_WLep_Z_WZ);

	      if( abs(truthW_WZ.CosTheta())<0.5 ){
		
		hist ("Cut4_WZ_pT")->Fill ((truthW+truthZ).Pt());
		hist ("Cut4_pT_W")->Fill (truthW.Pt());
		hist ("Cut4_pT_Z")->Fill (truthZ.Pt());
		hist ("Cut4_CosTheta_W_Lab")->Fill (truthW.CosTheta());
		hist ("Cut4_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
		hist ("Cut4_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
		hist ("Cut4_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
		hist ("Cut4_CosTheta_W_WZ_WZdir")->Fill (CosAngle_W_WZ_WZdir);
                hist ("Cut4_CosTheta_Z_WZ_WZdir")->Fill (CosAngle_Z_WZ_WZdir);
		hist ("Cut4_CosTheta_WLep_Wrest")->Fill (CosAngle_LepWrest_W);
		hist ("Cut4_CosTheta_WLep_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
		hist ("Cut4_CosTheta_ZLepM_Zrest")->Fill (CosAngle_LepMZrest_Z);
		hist ("Cut4_CosTheta_ZLepM_Zrest_wWZrest")->Fill (CosAngle_LepMZrest_Z_WZ);
		hist ("Cut4_all_Lep_pT")->Fill (truthWLep.Pt());
		hist ("Cut4_all_Lep_pT")->Fill (truthZLepM.Pt());
		hist ("Cut4_all_Lep_pT")->Fill (truthZLepP.Pt());
		hist ("Cut4_all_Lep_Eta")->Fill (truthWLep.Eta());
		hist ("Cut4_all_Lep_Eta")->Fill (truthZLepM.Eta());
		hist ("Cut4_all_Lep_Eta")->Fill (truthZLepP.Eta());
		hist ("Cut4_all_Lep_Phi")->Fill (truthWLep.Phi());
		hist ("Cut4_all_Lep_Phi")->Fill (truthZLepM.Phi());
		hist ("Cut4_all_Lep_Phi")->Fill (truthZLepP.Phi());
		hist ("Cut4_W_Rapidity_Lab")->Fill (W_Rapidity_Lab);
		hist ("Cut4_W_Rapidity_WZ")->Fill (W_Rapidity_WZ);
		hist ("Cut4_Z_Rapidity_Lab")->Fill (Z_Rapidity_Lab);
		hist ("Cut4_Z_Rapidity_WZ")->Fill (Z_Rapidity_WZ);
		hist ("Cut4_W_Lepton_Rapidity_Lab")->Fill (W_Lepton_Rapidity_Lab);
		hist ("Cut4_W_Lepton_Rapidity_WZ")->Fill (W_Lepton_Rapidity_WZ);
		hist ("Cut4_Z_NegLepton_Rapidity_Lab")->Fill (Z_NegLepton_Rapidity_Lab);
		hist ("Cut4_Z_NegLepton_Rapidity_WZ")->Fill (Z_NegLepton_Rapidity_WZ);
		hist ("Cut4_DiffRapidity_W_Z_Lab")->Fill (DiffRapidity_W_Z_Lab);
		hist ("Cut4_DiffRapidity_W_Z_WZ")->Fill (DiffRapidity_W_Z_WZ);
		hist ("Cut4_DiffRapidity_WLep_ZLepM_Lab")->Fill (DiffRapidity_WLep_ZLepM_Lab);
		hist ("Cut4_DiffRapidity_WLep_ZLepM_WZ")->Fill (DiffRapidity_WLep_ZLepM_WZ);
		hist ("Cut4_DiffRapidity_WLep_Z_Lab")->Fill (DiffRapidity_WLep_Z_Lab);
		hist ("Cut4_DiffRapidity_WLep_Z_WZ")->Fill (DiffRapidity_WLep_Z_WZ);
	      }
	    }
	  }
	}
	//}
	//}
      }
    }  
  }
    return StatusCode::SUCCESS;
}
  
bool MyxAODAnalysis_noChild :: SortTLorentz_AccPt(std::vector<TLorentzVector> &input_vec, std::vector<TLorentzVector> &sorted_vec){
  
  if(input_vec.size() == 0) return false;
  else{
    ANA_MSG_INFO("No of leptons selected: " << input_vec.size());
    for(int iInput=0; iInput < input_vec.size(); iInput++){
      ANA_MSG_INFO("iInput: " << iInput);
      double currentPt = input_vec[iInput].Pt();
      ANA_MSG_INFO("Input Particle Pt: " << currentPt);
      if(sorted_vec.size() == 0) sorted_vec.push_back(input_vec[iInput]);
      else{
	ANA_MSG_INFO("Particle Pt vector not empty: " << currentPt);
	ANA_MSG_INFO("Output vector size: " << sorted_vec.size());
	int curr_sorted_vec = sorted_vec.size();
	for(int iOutput=0; iOutput < curr_sorted_vec; ++iOutput){
	  ANA_MSG_INFO("iOutput: " << iOutput);
	  ANA_MSG_INFO("iOutput: " << iOutput);
	  if(currentPt >= sorted_vec[iOutput].Pt()){
	    ANA_MSG_INFO("if current pt is lesser: " << currentPt);
	    std::vector<TLorentzVector>::iterator it;
	    it = sorted_vec.begin()+iOutput;
	    sorted_vec.insert(it, input_vec[iInput]);
	    break;
	  }
	  else{
	    if(sorted_vec.size() ==1 || iOutput == curr_sorted_vec-1){
	      std::vector<TLorentzVector>::iterator it;
	      it = sorted_vec.begin()+iOutput+1;
	      sorted_vec.insert(it, input_vec[iInput]);
	      break;
	    }
	    else{
	      
	      if(currentPt < sorted_vec[iOutput].Pt() && currentPt >= sorted_vec[iOutput+1].Pt()){
		std::vector<TLorentzVector>::iterator it;
		it = sorted_vec.begin()+iOutput+1;
		sorted_vec.insert(it, input_vec[iInput]);
		break;
	      }
	      else 
		continue;
	      
	    }
	  }
	  
	}
      }
    }
    ANA_MSG_INFO("input vec size: " << input_vec.size());
    ANA_MSG_INFO("sorted vec size: " << sorted_vec.size());
    /*for(int ii=0; ii < sorted_vec.size(); ii++){
      ANA_MSG_INFO("Pt of sorted vector " << ii << ": " << sorted_vec[ii].Pt());
      }*/
    return true;
  }
}

bool MyxAODAnalysis_noChild :: SortTLorentz_AccPt(std::vector<Lepton> &input_vec, std::vector<Lepton> &sorted_vec){

  if(input_vec.size() == 0) return false;
  else{
    //ANA_MSG_INFO("No of leptons selected: " << input_vec.size());
    for(int iInput=0; iInput < input_vec.size(); iInput++){
      //ANA_MSG_INFO("iInput: " << iInput);
      double currentPt = input_vec[iInput].Pt();
      //ANA_MSG_INFO("Input Particle Pt: " << currentPt);
      if(sorted_vec.size() == 0) sorted_vec.push_back(input_vec[iInput]);
      else{
        //ANA_MSG_INFO("Particle Pt vector not empty: " << currentPt);
        //ANA_MSG_INFO("Output vector size: " << sorted_vec.size());
        int curr_sorted_vec = sorted_vec.size();
        for(int iOutput=0; iOutput < curr_sorted_vec; ++iOutput){
          //ANA_MSG_INFO("iOutput: " << iOutput);
          //ANA_MSG_INFO("iOutput: " << iOutput);
          if(currentPt >= sorted_vec[iOutput].Pt()){
            //ANA_MSG_INFO("if current pt is lesser: " << currentPt);
	    std::vector<Lepton>::iterator it;
	    it = sorted_vec.begin()+iOutput;
            sorted_vec.insert(it, input_vec[iInput]);
            break;
          }
          else{
            if(sorted_vec.size() ==1 || iOutput == curr_sorted_vec-1){
	      std::vector<Lepton>::iterator it;
              it = sorted_vec.begin()+iOutput+1;
              sorted_vec.insert(it, input_vec[iInput]);
              break;
            }
            else{

              if(currentPt < sorted_vec[iOutput].Pt() && currentPt >= sorted_vec[iOutput+1].Pt()){
		std::vector<Lepton>::iterator it;
                it = sorted_vec.begin()+iOutput+1;
                sorted_vec.insert(it, input_vec[iInput]);
                break;
              }
              else
                continue;

            }
          }

        }
      }
    }
    //ANA_MSG_INFO("input vec size: " << input_vec.size());
    //ANA_MSG_INFO("sorted vec size: " << sorted_vec.size());
    /*for(int ii=0; ii < sorted_vec.size(); ii++){   
      ANA_MSG_INFO("Pt of sorted vector " << ii << ": " << sorted_vec[ii].Pt());
      }*/
    return true;
  }
}

StatusCode MyxAODAnalysis_noChild :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  /*TF1 *f1 = new TF1("f_pol_frac","((3/8)*[0]*(1-x)^2) + ((3/8)*[1]*(1+x)^2) + ((3/4)*[2]*(1-x^2))",-1,1);

  hist ("CosTheta_WLep_Wrest_wWZrest")->Fit("f_pol_frac","Q");

  TF1 *fit = hist ("CosTheta_WLep_Wrest_wWZrest")->GetFunction("f_pol_frac");

  ANA_MSG_INFO ("execute(): fL "<<fit->GetParameter(0)<<", fR "<<fit->GetParameter(1)<<", f0 "<<fit->GetParameter(2));*/

  return StatusCode::SUCCESS;
}
