#include <AsgTools/MessageCheck.h>
#include <xAODJet/JetContainer.h>
//#include <xAODMissingET/MissingETContainer.h>                                                                                                                                                             

//======== Adding TruthVertex headers ========//                                                                                                                                                            

#include <xAODTruth/TruthVertexContainer.h>

//============== For AnaToolHandle ===============//                                                                                                                                                        

#include <TSystem.h>

#include <TLorentzVector.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>

#include <xAODTruth/TruthParticleContainer.h>
#include <TLorentzVector.h>
#include <cmath>
#include <vector>
#include <TH1.h>
#include "TF1.h"

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,"Minimum electron pT (in MeV)" );
  declareProperty( "SampleName", m_sampleName = "Unknown","Descriptive name for the processed sample" );

}

MyxAODAnalysis :: ~MyxAODAnalysis () {

  delete m_particleStatus;
  delete m_particlePdgID;
  delete m_particleEta;
  delete m_particlePhi;
  delete m_particlePt;
  delete m_particleE;
  delete m_particlePx;
  delete m_particlePy;
  delete m_particlePz;

}


StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO ("in initialize");

  //============ Making N-tuples ========//                                                                                                           

  //ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  //TTree* mytree = tree ("analysis");
  //mytree->Branch ("RunNumber", &m_runNumber);
  //mytree->Branch ("EventNumber", &m_eventNumber);

  //=========== Particles                                                                                                                                       

  m_particleStatus = new std::vector<float>();
  m_particlePdgID = new std::vector<float>();
  m_particleEta = new std::vector<float>();
  m_particlePhi = new std::vector<float>();
  m_particlePt = new std::vector<float>();
  m_particleE = new std::vector<float>();
  m_particlePx = new std::vector<float>();
  m_particlePy = new std::vector<float>();
  m_particlePz = new std::vector<float>();

  //mytree->Branch ("ParticleStatus", &m_particleStatus);
  //mytree->Branch ("ParticlePdgID", &m_particlePdgID);
  //mytree->Branch ("ParticleEta", &m_particleEta);
  //mytree->Branch ("ParticlePhi", &m_particlePhi);
  //mytree->Branch ("ParticlePt", &m_particlePt);
  //mytree->Branch ("ParticleE", &m_particleE);
  //mytree->Branch ("ParticlePx", &m_particlePx);
  //mytree->Branch ("ParticlePy", &m_particlePy);
  //mytree->Branch ("ParticlePz", &m_particlePz);

  //======================= Booking Histograms ============================//

  //ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]


  ANA_CHECK (book (TH1F("InvMass_W","InvMass_W",100,0,250)));
  ANA_CHECK (book (TH1F("InvMass_Z","InvMass_Z",100,0,250)));
  ANA_CHECK (book (TH1F("InvMass_WZ","InvMass_WZ",100,0,1000)));
  ANA_CHECK (book (TH1F("WZ_pT", "WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F("WZ_pT_W_Z", "WZ_pT_W_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F("pT_W", "pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F("pT_Z", "pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F("CosTheta_W_Lab","CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_Z_Lab","CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_W_WZ","CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_Z_WZ","CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_WLepNeu_Wrest","CosTheta_WLepNeu_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F("CosTheta_WLepNeu_Wrest_wWZrest","CosTheta_WLepNeu_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F("all_Lep_pT","all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F("all_Lep_Eta","all_Lep_Eta",50,-10,10)));

  ANA_CHECK (book (TH1F ("Cut1_WZ_pT", "Cut1_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut1_pT_W", "Cut1_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut1_pT_Z", "Cut1_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_W_Lab", "Cut1_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_Z_Lab", "Cut1_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_W_WZ", "Cut1_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_Z_WZ", "Cut1_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_WLepNeu_Wrest", "Cut1_CosTheta_WLepNeu_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_CosTheta_WLepNeu_Wrest_wWZrest", "Cut1_CosTheta_WLepNeu_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut1_all_Lep_pT", "Cut1_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut1_all_Lep_Eta", "Cut1_all_Lep_Eta",50,-10,10)));
  
  ANA_CHECK (book (TH1F ("Cut2_WZ_pT", "Cut2_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut2_pT_W", "Cut2_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut2_pT_Z", "Cut2_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_W_Lab", "Cut2_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_Z_Lab", "Cut2_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_W_WZ", "Cut2_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_Z_WZ", "Cut2_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_WLepNeu_Wrest", "Cut2_CosTheta_WLepNeu_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_CosTheta_WLepNeu_Wrest_wWZrest", "Cut2_CosTheta_WLepNeu_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut2_all_Lep_pT", "Cut2_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut2_all_Lep_Eta", "Cut2_all_Lep_Eta",50,-10,10)));
  
  ANA_CHECK (book (TH1F ("Cut3_WZ_pT", "Cut3_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut3_pT_W", "Cut3_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut3_pT_Z", "Cut3_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_W_Lab", "Cut3_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_Z_Lab", "Cut3_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_W_WZ", "Cut3_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_Z_WZ", "Cut3_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_WLepNeu_Wrest", "Cut3_CosTheta_WLepNeu_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_CosTheta_WLepNeu_Wrest_wWZrest", "Cut3_CosTheta_WLepNeu_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut3_all_Lep_pT", "Cut3_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut3_all_Lep_Eta", "Cut3_all_Lep_Eta",50,-10,10)));

  ANA_CHECK (book (TH1F ("Cut4_WZ_pT", "Cut4_WZ_pT", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut4_pT_W", "Cut4_pT_W", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut4_pT_Z", "Cut4_pT_Z", 100, 0, 1000)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_W_Lab", "Cut4_CosTheta_W_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_Z_Lab", "Cut4_CosTheta_Z_Lab",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_W_WZ", "Cut4_CosTheta_W_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_Z_WZ", "Cut4_CosTheta_Z_WZ",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_WLepNeu_Wrest", "Cut4_CosTheta_WLepNeu_Wrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_CosTheta_WLepNeu_Wrest_wWZrest", "Cut4_CosTheta_WLepNeu_Wrest_wWZrest",20,-1,1)));
  ANA_CHECK (book (TH1F ("Cut4_all_Lep_pT", "Cut4_all_Lep_pT",100,0,1000)));
  ANA_CHECK (book (TH1F ("Cut4_all_Lep_Eta", "Cut4_all_Lep_Eta",50,-10,10)));

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //ANA_MSG_INFO ("in execute");

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  //ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  const xAOD::TruthParticleContainer* truthParts;                                                                                                                              
  ANA_CHECK (evtStore()->retrieve( truthParts, "TruthParticles"));                                                                                                                  
  //ANA_MSG_INFO ("execute(): size of TruthParts Container = " << truthParts->size());                                                                                       
  for (const xAOD::TruthParticle *truthPart : *truthParts) {                                                 
    
    ANA_MSG_INFO ("execute(): truthPart->pdgId() " << truthPart->pdgId());
                                                        
    if (abs(truthPart->pdgId()) == 24 || abs(truthPart->pdgId()) == 23){ //&& truthPart->status() == 62) {                                                                       
      ANA_MSG_INFO ("execute(): Particle status = " << (truthPart->status()));                                                         
      ANA_MSG_INFO ("execute(): Particle pdgID = " << (truthPart->pdgId()) );              
      
      
      //=========== decay vertex information ==============//                                                                                          
      //bool hasDecayVertex = truthBoson->hasDecayVtx();                                                                                                     
      //ANA_MSG_INFO ("execute(): Boson has a decay vertex ? "<< hasDecayVertex );                                                                      
      //const xAOD::TruthVertex* decayVtxBoson = truthBoson->decayVtx();                                                                                      
      //const xAOD::TruthParticle* decayParticle = decayVtxBoson->outgoingParticle(0);                                                                                 
      
      //============== Children information ================//                                                                                                                       
      size_t No_Children = truthPart->nChildren();                                                                                                                                                        
      ANA_MSG_INFO ("execute(): Particle children ? "<< No_Children );                                                                                                                                    
      
      if(No_Children>=2){
	const xAOD::TruthParticle *truthPart_Child_1 = truthPart->child(0);                                                                                                    
	const xAOD::TruthParticle *truthPart_Child_2 = truthPart->child(1);                                                                                                         
	ANA_MSG_INFO ("execute(): Particle child_1 = " << truthPart_Child_1->pdgId() << " pT = " << (truthPart_Child_1->pt()*0.001));                                          
	ANA_MSG_INFO ("execute(): Particle child_2 = " << truthPart_Child_2->pdgId() << " pT = " << (truthPart_Child_2->pt()*0.001));
      }
    }
  }

  //============Using TruthParticles ===============//                                                                                                         

  // Read/fill the EventInfo variables:                                                                                                                              
  const xAOD::EventInfo* ei = nullptr;
  ANA_CHECK (evtStore()->retrieve (ei, "EventInfo"));
  m_runNumber = ei->runNumber ();
  m_eventNumber = ei->eventNumber ();

  m_particleStatus->clear();
  m_particlePdgID->clear();
  m_particleEta->clear();
  m_particlePhi->clear();
  m_particlePt->clear();
  m_particleE->clear();
  m_particlePx->clear();
  m_particlePy->clear();
  m_particlePz->clear();

  // Read/fill the electron variables:                                                                                                                                                  
  /*
  int num_W=0, num_Z=0;

  const xAOD::TruthParticleContainer* particles = nullptr;
  ANA_CHECK (evtStore()->retrieve (particles, "TruthParticles"));
  for (const xAOD::TruthParticle* particle : *particles) {
    if(abs(particle->pdgId())==24 && particle->status()==22) num_W+=1;
    if(abs(particle->pdgId())==23 && particle->status()==22) num_Z+=1;
  }

  const xAOD::TruthParticle *particle_Child_1;
  const xAOD::TruthParticle *particle_Child_2;
  const xAOD::TruthParticle *particle_Child_3;
  const xAOD::TruthParticle *particle_Child_4;

  TLorentzVector truthWLep, truthWNeu, truthZLepM, truthZLepP, truthW, truthZ, truthWZ,  truthW_org, truthZ_org;
  int truthWLep_pdgid, truthWNeu_pdgid, truthZLepM_pdgid, truthZLepP_pdgid, truthW_pdgid, truthZ_pdgid;

  if( num_W==1 && num_Z==1 ){
    for (const xAOD::TruthParticle* particle : *particles) {
      if( (abs(particle->pdgId())==24 || abs(particle->pdgId())==23) ){
        if(particle->status()==22 ){
          //if(particle->nChildren()!=2 && particle->nChildren()!=4) {ANA_MSG_INFO ("execute(): Noooooo No. of children "<<particle->nChildren()); continue;}
          //ANA_MSG_INFO ("execute(): Particle Status "<<particle->status()<<" particle pid "<<particle->pdgId()<<" particle px: "<<(particle->px()*0.001) << " particle py: "<<(particle->py()*0.001) << " particle pz: "<<(particle->pz()*0.001) );
	  //if( abs(particle->pdgId())==24 ){
            //truthW_org.SetPxPyPzE( (particle->px()*0.001), (particle->py()*0.001), (particle->pz()*0.001), (particle->e()*0.001) );
	  //}
	  //else if( abs(particle->pdgId())==23 ){
	    //truthZ_org.SetPxPyPzE( (particle->px()*0.001), (particle->py()*0.001), (particle->pz()*0.001), (particle->e()*0.001) );
	    //}
	  //ANA_MSG_INFO ("execute(): Pt WZ " << (truthW_org+truthZ_org).Pt()); 
          m_particleStatus->push_back (particle->status());
          m_particlePdgID->push_back(particle->pdgId());
          m_particleEta->push_back (particle->eta ());
          m_particlePhi->push_back (particle->phi ());
          m_particlePt-> push_back ((particle->pt ())*0.001);
          m_particleE->  push_back ((particle->e ())*0.001);
          m_particlePx-> push_back ((particle->px ())*0.001);
          m_particlePy-> push_back ((particle->py ())*0.001);
          m_particlePz-> push_back ((particle->pz ())*0.001);
        }
        else if(particle->status()==62){
          //if(particle->nChildren()!=2 && particle->nChildren()!=4) {ANA_MSG_INFO ("execute(): Noooooo No. of children "<<particle->nChildren()); continue;}
          //ANA_MSG_INFO ("execute(): Particle Status "<<particle->status()<<" particle pid "<<particle->pdgId()<<" particle px: "<<(particle->px()*0.001));
	  //if(abs(particle->pdgId())==24) truthW_org.SetPxPyPzE( (particle->px()*0.001), (particle->py()*0.001), (particle->pz()*0.001), (particle->e()*0.001) );
	  //else if(abs(particle->pdgId())==23) truthZ_org.SetPxPyPzE( (particle->px()*0.001), (particle->py()*0.001), (particle->pz()*0.001), (particle->e()*0.001) );
	  if(particle->nChildren()==2){
            particle_Child_1 = particle->child(0);
            particle_Child_2 = particle->child(1);
            //ANA_MSG_INFO ("execute(): Particle child_1 = " << particle_Child_1->pdgId() << " px = " << (particle_Child_1->px()*0.001));
            //ANA_MSG_INFO ("execute(): Particle child_2 = " << particle_Child_2->pdgId() << " px = " << (particle_Child_2->px()*0.001));
          }
          else if(particle->nChildren()==4){
            ANA_MSG_INFO ("execute(): BLAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
            if(abs(particle->pdgId())!=24) ANA_MSG_INFO ("execute(): NOOOOOOO :(");
            particle_Child_1 = particle->child(0);
            particle_Child_2 = particle->child(1);
            particle_Child_3 = particle->child(2);
            particle_Child_4 = particle->child(3);
            //ANA_MSG_INFO ("execute(): Particle child_1 = " << particle_Child_1->pdgId() << " px = " << (particle_Child_1->px()*0.001));
            //ANA_MSG_INFO ("execute(): Particle child_2 = " << particle_Child_2->pdgId() << " px = " << (particle_Child_2->px()*0.001));
            //ANA_MSG_INFO ("execute(): Particle child_3 = " << particle_Child_3->pdgId() << " px = " << (particle_Child_3->px()*0.001));
            //ANA_MSG_INFO ("execute(): Particle child_4 = " << particle_Child_4->pdgId() << " px = " << (particle_Child_4->px()*0.001));
          }
	  else if(particle->nChildren()==3){
            if(abs(particle->pdgId())!=24) ANA_MSG_INFO ("execute(): NOOOOOOO :(");
            particle_Child_1 = particle->child(0);
            particle_Child_2 = particle->child(1);
            particle_Child_3 = particle->child(2);
            //ANA_MSG_INFO ("execute(): Particle Status "<<particle->status()<<" particle pid "<<particle->pdgId()<<" particle px: "<<(particle->px()*0.001));
            //ANA_MSG_INFO ("execute(): Particle child_1 = " << particle_Child_1->pdgId() << " px = " << (particle_Child_1->px()*0.001));
            //ANA_MSG_INFO ("execute(): Particle child_2 = " << particle_Child_2->pdgId() << " px = " << (particle_Child_2->px()*0.001));
            //ANA_MSG_INFO ("execute(): Particle child_3 = " << particle_Child_3->pdgId() << " px = " << (particle_Child_3->px()*0.001));
          }
          else{ANA_MSG_INFO ("execute(): events which are not considered. No of children "<<particle->nChildren());}
          m_particleStatus->push_back (particle_Child_1->status());
          m_particlePdgID->push_back(particle_Child_1->pdgId());
          m_particleEta->push_back (particle_Child_1->eta ());
          m_particlePhi->push_back (particle_Child_1->phi ());
          m_particlePt-> push_back ((particle_Child_1->pt ())*0.001);
          m_particleE->  push_back ((particle_Child_1->e ())*0.001);
          m_particlePx-> push_back ((particle_Child_1->px ())*0.001);
          m_particlePy-> push_back ((particle_Child_1->py ())*0.001);
          m_particlePz-> push_back ((particle_Child_1->pz ())*0.001);

          m_particleStatus->push_back (particle_Child_2->status());
          m_particlePdgID->push_back(particle_Child_2->pdgId());
          m_particleEta->push_back (particle_Child_2->eta ());
          m_particlePhi->push_back (particle_Child_2->phi ());
          m_particlePt-> push_back ((particle_Child_2->pt ())*0.001);
          m_particleE->  push_back ((particle_Child_2->e ())*0.001);
          m_particlePx-> push_back ((particle_Child_2->px ())*0.001);
          m_particlePy-> push_back ((particle_Child_2->py ())*0.001);
          m_particlePz-> push_back ((particle_Child_2->pz ())*0.001);
	  

	  //========================================================== storing all the leptons ===================================//
	  
	  if( abs(particle->pdgId())==24 ){
	    truthW_org.SetPxPyPzE( (particle->px()*0.001), (particle->py()*0.001), (particle->pz()*0.001), (particle->e()*0.001) );
	    truthW_pdgid=particle->pdgId();
	    if( abs(particle_Child_1->pdgId())==11 || abs(particle_Child_1->pdgId())==13 || abs(particle_Child_1->pdgId())==15 ){
	      truthWLep.SetPxPyPzE( (particle_Child_1->px()*0.001), (particle_Child_1->py()*0.001), (particle_Child_1->pz()*0.001), (particle_Child_1->e()*0.001) );
	      //ANA_MSG_INFO ("execute(): WLep Px Py Pz "<<particle_Child_1->px()*0.001<<" "<<particle_Child_1->py()*0.001<<" "<<particle_Child_1->pz()*0.001);
	      truthWLep_pdgid = particle_Child_1->pdgId();
	    }
	    if( abs(particle_Child_2->pdgId())==11 || abs(particle_Child_2->pdgId())==13 || abs(particle_Child_2->pdgId())==15 ){
	      truthWLep.SetPxPyPzE( (particle_Child_2->px()*0.001), (particle_Child_2->py()*0.001), (particle_Child_2->pz()*0.001), (particle_Child_2->e()*0.001) );
	      truthWLep_pdgid = particle_Child_2->pdgId();
	    }
	    if( abs(particle_Child_1->pdgId())==12 || abs(particle_Child_1->pdgId())==14 || abs(particle_Child_1->pdgId())==16 ){
	      truthWNeu.SetPxPyPzE( (particle_Child_1->px()*0.001), (particle_Child_1->py()*0.001), (particle_Child_1->pz()*0.001), (particle_Child_1->e()*0.001) );
	      truthWNeu_pdgid = particle_Child_1->pdgId();
	    }
	    if( abs(particle_Child_2->pdgId())==12 || abs(particle_Child_2->pdgId())==14 || abs(particle_Child_2->pdgId())==16 ){
	      truthWNeu.SetPxPyPzE( (particle_Child_2->px()*0.001), (particle_Child_2->py()*0.001), (particle_Child_2->pz()*0.001), (particle_Child_2->e()*0.001) );
	      //ANA_MSG_INFO ("execute(): WNeu Px Py Pz "<<particle_Child_2->px()*0.001<<" "<<particle_Child_2->py()*0.001<<" "<<particle_Child_2->pz()*0.001);
	      truthWNeu_pdgid = particle_Child_2->pdgId();
	    }
	  }
	  else if( abs(particle->pdgId())==23 ){
	    truthZ_org.SetPxPyPzE( (particle->px()*0.001), (particle->py()*0.001), (particle->pz()*0.001), (particle->e()*0.001) );
	    truthZ_pdgid=particle->pdgId();
	    if( particle_Child_1->pdgId()==11 || particle_Child_1->pdgId()==13 || particle_Child_1->pdgId()==15 ){
	      truthZLepM.SetPxPyPzE( (particle_Child_1->px()*0.001), (particle_Child_1->py()*0.001), (particle_Child_1->pz()*0.001), (particle_Child_1->e()*0.001) );
	      truthZLepM_pdgid = particle_Child_1->pdgId();
	    }   
            if( particle_Child_2->pdgId()==11 || particle_Child_2->pdgId()==13 || particle_Child_2->pdgId()==15 ){
	      truthZLepM.SetPxPyPzE( (particle_Child_2->px()*0.001), (particle_Child_2->py()*0.001), (particle_Child_2->pz()*0.001), (particle_Child_2->e()*0.001) );
	      truthZLepM_pdgid = particle_Child_2->pdgId();
	    }   
            if( particle_Child_1->pdgId()==-11 || particle_Child_1->pdgId()==-13 || particle_Child_1->pdgId()==-15 ){
	      truthZLepP.SetPxPyPzE( (particle_Child_1->px()*0.001), (particle_Child_1->py()*0.001), (particle_Child_1->pz()*0.001), (particle_Child_1->e()*0.001) );
	      truthZLepP_pdgid = particle_Child_1->pdgId();
            }
            if( particle_Child_2->pdgId()==-11 || particle_Child_2->pdgId()==-13 || particle_Child_2->pdgId()==-15 ){
	      truthZLepP.SetPxPyPzE( (particle_Child_2->px()*0.001), (particle_Child_2->py()*0.001), (particle_Child_2->pz()*0.001), (particle_Child_2->e()*0.001) );
	      //ANA_MSG_INFO ("execute(): ZLepP Px Py Pz "<<particle_Child_2->px()*0.001<<" "<<particle_Child_2->py()*0.001<<" "<<particle_Child_2->pz()*0.001);
	      truthZLepP_pdgid = particle_Child_2->pdgId();
            }
          }

        }
      }
    }
  }
  
  //ANA_MSG_INFO ("execute(): ZLepP Px Py Pz "<<truthZLepP.Px()<<" "<<truthZLepP.Py()<<" "<<truthZLepP.Pz());

  truthW = truthWLep + truthWNeu;
  truthZ = truthZLepM + truthZLepP;
  truthWZ = truthW + truthZ;

  ANA_MSG_INFO ("execute(): Pt WZ with W and Z" << (truthW_org+truthZ_org).Pt());
  ANA_MSG_INFO ("execute(): Pt WZ with leptons" << (truthW+truthZ).Pt());

  TLorentzVector truthWLep_WZ, truthWNeu_WZ, truthZLepM_WZ, truthZLepP_WZ, truthW_WZ, truthZ_WZ;
  
  truthWLep_WZ = truthWLep;
  truthWLep_WZ.Boost((-1)*truthWZ.BoostVector());
  
  truthWNeu_WZ = truthWNeu;
  truthWNeu_WZ.Boost((-1)*truthWZ.BoostVector());
  
  truthZLepM_WZ = truthZLepM;
  truthZLepM_WZ.Boost((-1)*truthWZ.BoostVector());
  
  truthZLepP_WZ = truthZLepP;
  truthZLepP_WZ.Boost((-1)*truthWZ.BoostVector());
  
  truthW_WZ = truthW;
  truthW_WZ.Boost((-1)*truthWZ.BoostVector());
  
  truthZ_WZ = truthZ;
  truthZ_WZ.Boost((-1)*truthWZ.BoostVector());
  
  double CosAngle_LepWrest_W, CosAngle_LepWrest_W_WZ;
  
  TLorentzVector Lep_WFrame = truthWLep;
  Lep_WFrame.Boost((-1)*truthW.BoostVector());
  CosAngle_LepWrest_W=cos((Lep_WFrame.Vect()).Angle(truthW.Vect()));

  TLorentzVector Lep_WFrame_WZ = truthWLep_WZ;
  Lep_WFrame_WZ.Boost((-1)*truthW_WZ.BoostVector());
  CosAngle_LepWrest_W_WZ=cos((Lep_WFrame_WZ.Vect()).Angle(truthW_WZ.Vect()));

  //tree ("analysis")->Fill ();

  //================================== Filling Histograms ============================================//

  if( truthZ.M() >60 && truthW_pdgid==24){

    //ANA_MSG_INFO ("execute():Children pdgid "<<truthWLep_pdgid<<" "<<truthWNeu_pdgid<<" "<<truthZLepM_pdgid<<" "<<truthZLepP_pdgid);

    if( !( abs(truthWLep_pdgid)==15 || abs(truthWNeu_pdgid)==16 || abs(truthZLepM_pdgid)==15 || abs(truthZLepP_pdgid)==15 ) ){
      
      hist ("InvMass_W")->Fill (truthW.M());
      hist ("InvMass_Z")->Fill (truthZ.M());
      hist ("InvMass_WZ")->Fill (truthWZ.M());
      hist ("WZ_pT")->Fill ((truthW+truthZ).Pt());
      hist ("WZ_pT_W_Z")->Fill ((truthW_org+truthZ_org).Pt());
      hist ("pT_W")->Fill (truthW.Pt());
      hist ("pT_Z")->Fill (truthZ.Pt());
      hist ("CosTheta_W_Lab")->Fill (truthW.CosTheta());
      hist ("CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
      hist ("CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
      hist ("CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
      hist ("CosTheta_WLepNeu_Wrest")->Fill (CosAngle_LepWrest_W);
      hist ("CosTheta_WLepNeu_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
      hist ("all_Lep_pT")->Fill (truthWLep.Pt());
      hist ("all_Lep_pT")->Fill (truthZLepM.Pt());
      hist ("all_Lep_pT")->Fill (truthZLepP.Pt());
      hist ("all_Lep_Eta")->Fill (truthWLep.Eta());
      hist ("all_Lep_Eta")->Fill (truthZLepM.Eta());
      hist ("all_Lep_Eta")->Fill (truthZLepP.Eta());
      
      //    ANA_MSG_INFO ("execute(): truthWLep.Pt() truthZLepP.Pt() truthZLepM.Pt() "<<truthWLep.Pt()<<" "<<truthZLepP.Pt()<<" "<<truthZLepM.Pt());
      
      if( truthWLep.Pt()>25 && truthZLepP.Pt()>25 && truthZLepM.Pt()>25 && truthWNeu.Pt()>25 && abs(truthWLep.Eta())<2.5 && abs(truthZLepP.Eta())<2.5 && abs(truthZLepM.Eta())<2.5 ){
	
	//ANA_MSG_INFO ("execute(): Woahhhhhhhhh ");
	
	hist ("Cut1_WZ_pT")->Fill ((truthW+truthZ).Pt());
	hist ("Cut1_pT_W")->Fill (truthW.Pt());
	hist ("Cut1_pT_Z")->Fill (truthZ.Pt());
	hist ("Cut1_CosTheta_W_Lab")->Fill (truthW.CosTheta());
	hist ("Cut1_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	hist ("Cut1_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	hist ("Cut1_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	hist ("Cut1_CosTheta_WLepNeu_Wrest")->Fill (CosAngle_LepWrest_W);
	hist ("Cut1_CosTheta_WLepNeu_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	hist ("Cut1_all_Lep_pT")->Fill (truthWLep.Pt());
	hist ("Cut1_all_Lep_pT")->Fill (truthZLepM.Pt());
	hist ("Cut1_all_Lep_pT")->Fill (truthZLepP.Pt());
	hist ("Cut1_all_Lep_Eta")->Fill (truthWLep.Eta());
	hist ("Cut1_all_Lep_Eta")->Fill (truthZLepM.Eta());
	hist ("Cut1_all_Lep_Eta")->Fill (truthZLepP.Eta());
	
	if( truthZ.Pt()>200 ){
	  
	  hist ("Cut2_WZ_pT")->Fill ((truthW+truthZ).Pt());
	  hist ("Cut2_pT_W")->Fill (truthW.Pt());
	  hist ("Cut2_pT_Z")->Fill (truthZ.Pt());
	  hist ("Cut2_CosTheta_W_Lab")->Fill (truthW.CosTheta());
	  hist ("Cut2_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	  hist ("Cut2_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	  hist ("Cut2_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	  hist ("Cut2_CosTheta_WLepNeu_Wrest")->Fill (CosAngle_LepWrest_W);
	  hist ("Cut2_CosTheta_WLepNeu_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	  hist ("Cut2_all_Lep_pT")->Fill (truthWLep.Pt());
	  hist ("Cut2_all_Lep_pT")->Fill (truthZLepM.Pt());
	  hist ("Cut2_all_Lep_pT")->Fill (truthZLepP.Pt());
	  hist ("Cut2_all_Lep_Eta")->Fill (truthWLep.Eta());
	  hist ("Cut2_all_Lep_Eta")->Fill (truthZLepM.Eta());
	  hist ("Cut2_all_Lep_Eta")->Fill (truthZLepP.Eta());
	  
	  if( truthWZ.Pt()<70 ){
	    
	    hist ("Cut3_WZ_pT")->Fill ((truthW+truthZ).Pt());
	    hist ("Cut3_pT_W")->Fill (truthW.Pt());
	    hist ("Cut3_pT_Z")->Fill (truthZ.Pt());
	    hist ("Cut3_CosTheta_W_Lab")->Fill (truthW.CosTheta());
	    hist ("Cut3_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	    hist ("Cut3_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	    hist ("Cut3_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	    hist ("Cut3_CosTheta_WLepNeu_Wrest")->Fill (CosAngle_LepWrest_W);
	    hist ("Cut3_CosTheta_WLepNeu_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	    hist ("Cut3_all_Lep_pT")->Fill (truthWLep.Pt());
	    hist ("Cut3_all_Lep_pT")->Fill (truthZLepM.Pt());
	    hist ("Cut3_all_Lep_pT")->Fill (truthZLepP.Pt());
	    hist ("Cut3_all_Lep_Eta")->Fill (truthWLep.Eta());
	    hist ("Cut3_all_Lep_Eta")->Fill (truthZLepM.Eta());
	    hist ("Cut3_all_Lep_Eta")->Fill (truthZLepP.Eta());
	    
	    if( abs(truthW_WZ.CosTheta())<0.5 ){
	    
	      hist ("Cut4_WZ_pT")->Fill ((truthW+truthZ).Pt());
	      hist ("Cut4_pT_W")->Fill (truthW.Pt());
	      hist ("Cut4_pT_Z")->Fill (truthZ.Pt());
	      hist ("Cut4_CosTheta_W_Lab")->Fill (truthW.CosTheta());
	      hist ("Cut4_CosTheta_Z_Lab")->Fill (truthZ.CosTheta());
	      hist ("Cut4_CosTheta_W_WZ")->Fill (truthW_WZ.CosTheta());
	      hist ("Cut4_CosTheta_Z_WZ")->Fill (truthZ_WZ.CosTheta());
	      hist ("Cut4_CosTheta_WLepNeu_Wrest")->Fill (CosAngle_LepWrest_W);
	      hist ("Cut4_CosTheta_WLepNeu_Wrest_wWZrest")->Fill (CosAngle_LepWrest_W_WZ);
	      hist ("Cut4_all_Lep_pT")->Fill (truthWLep.Pt());
	      hist ("Cut4_all_Lep_pT")->Fill (truthZLepM.Pt());
	      hist ("Cut4_all_Lep_pT")->Fill (truthZLepP.Pt());
	      hist ("Cut4_all_Lep_Eta")->Fill (truthWLep.Eta());
	      hist ("Cut4_all_Lep_Eta")->Fill (truthZLepM.Eta());
	      hist ("Cut4_all_Lep_Eta")->Fill (truthZLepP.Eta());
	      
	    }
	  }
	}
      }
    }
  }*/
 
  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  /*TF1 *f1 = new TF1("f_pol_frac","((3/8)*[0]*(1-x)^2) + ((3/8)*[1]*(1+x)^2) + ((3/4)*[2]*(1-x^2))",-1,1);

  hist ("CosTheta_WLepNeu_Wrest_wWZrest")->Fit("f_pol_frac","Q");

  TF1 *fit = hist ("CosTheta_WLepNeu_Wrest_wWZrest")->GetFunction("f_pol_frac");

  ANA_MSG_INFO ("execute(): fL "<<fit->GetParameter(0)<<", fR "<<fit->GetParameter(1)<<", f0 "<<fit->GetParameter(2));*/

  return StatusCode::SUCCESS;
}
