Execute the following commands:

( This package is built using the following ATLAS analysis tutorial. Please have a look at the link for more details: https://atlassoftwaredocs.web.cern.ch/ABtutorial/ )

```bash
git clone < url >
cd truth1_xaod_analysis
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
mkdir build
mkdir run
cd build
asetup 21.2.104,AnalysisBase
cmake ../source/
make
source x86_64-*/setup.sh
```

After Every login: 

```bash
cd build
asetup --restore
source x86_64-*/setup.sh
```
Or You can also just do when you are revisiting the setup:
```bash
source ../setupBuild.sh
```

Also export the directory as the one which contains all the xAODs:

```bash
export TRUTH_xAOD_Path=<directory containing TRUTH1 xAOD root files>
```

To execute the script:

```bash
cd ../run
root -b -q '$ROOTCOREDIR/scripts/load_packages.C' '../source/MyAnalysis/share/ATestRun_eljob.cxx ("<Name of the output directory>")'
```

NTuples are also generated and stored in the directory called "data-ANALYSIS". In this root file, leptons are stored (e, mu) which has pT > 15 GeV and absEta < 2.5. 

Changes that have to be done in ../source/MyAnalysis/share/ATestRun_eljob.cxx:

1. In source/MyAnalysis/Root/ there are two classes MyxAODAnalysis and MyxAODAnalysis_noChild. 

MyxAODAnalysis finds out the leptons by looking at the child information of W and Z.

MyxAODAnalysis_noChild does not look at the child information and finds out the leptons by applying some cuts.

So in the file ATestRun_eljob.cxx select the line :

alg.setType ("MyxAODAnalysis"); or alg.setType ("MyxAODAnalysis_noChild"); 

according to the requirements.

2. To set the max number of events you want to analyse use this :

job.options()->setDouble (EL::Job::optMaxEvents, '<Max_number_of_events_you_want_to_analyse>');

