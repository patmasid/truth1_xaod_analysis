export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
asetup --restore
source x86_64-*/setup.sh
export TRUTH_xAOD_Path=/lustre/umt3/user/patmasid/TRUTH1_.364253.Sherpa_222_NNPDF30NNLO_lllv.merge.EVNT.e5916_e5984
